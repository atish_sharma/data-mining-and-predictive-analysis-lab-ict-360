from itertools import combinations, chain

class apriori:

	def __init__(self,transactions=[],sup_count=0):
		self.transactions=transactions
		self.transaction_map={}
		self.transaction_map_rev={}
		id,r,c=0,0,0
		for i in self.transactions:
			c=0
			for j in i:
				if j not in self.transaction_map:
					self.transaction_map[j]=id
					self.transaction_map_rev[id]=j
					id+=1
				self.transactions[r][c]=self.transaction_map[j]
				c+=1
			r+=1
		self.sup_count=sup_count
		self.frequent_itemset=set()

	def generate_frequent_itemsets(self):
		candidate_set=self.get_candidate_set()
		count=1
		while len(candidate_set)>0:
			pruned_itemset=set()
			if count>1:
				for i in candidate_set:
					if not self.prune(i):
						pruned_itemset.add(i)
			else:
				pruned_itemset=candidate_set
			frequent_itemset=self.get_frequent_itemset(pruned_itemset)
			self.frequent_itemset|=frequent_itemset
			count+=1
			candidate_set=self.get_candidate_set(frequent_itemset,count)
		confirmed_frequent_itemset=[]
		for i in self.frequent_itemset:
			l=[]
			for j in i:
				l.append(self.transaction_map_rev[j])
			confirmed_frequent_itemset.append(l)
		return confirmed_frequent_itemset
			
		
	def get_candidate_set(self,prev_set=set(),count=1):
		candidate_set=set()
		if count==1:
			for i in self.transactions:
				for j in i:
					candidate_set.add(frozenset([j]))
		else:
			for i in prev_set:
				for j in prev_set:
					s=i|j
					if len(s)==count:
						candidate_set.add(s)
		return candidate_set

	def get_frequent_itemset(self,candidate_set=set()):
			del_list=[]
			for i in candidate_set:
				if self.get_sup_count(i)<self.sup_count:
					del_list.append(i)
			for i in del_list:
				candidate_set.remove(i)
			return candidate_set

	def get_sup_count(self,item_set):
		count=0.0
		for i in self.transactions:
			if set(i).issuperset(item_set):
				count+=1.0
		return count/len(self.transactions)

	def powerset(self,iterable):
    		s=list(iterable)
    		return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

	def prune(self,item_set):
		ps=list(self.powerset(list(item_set)))
		del_list=[]
		for i in range(len(ps)):
			if len(ps[i])==0 or len(ps[i])==len(item_set):
				del_list.append(i)
		for i in range(len(del_list)-1,-1,-1):
			del ps[del_list[i]]
		for i in ps:
			if set(i) not in self.frequent_itemset:
				return True
		return False
