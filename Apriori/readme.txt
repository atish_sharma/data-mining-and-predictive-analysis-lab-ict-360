apriori.py : contains apriori class.
apriori_test.py : instantiates an object of apriori class and feeds it input from the command line.
data_set.txt : example data set with a list of transactions.

How to run apriori_test.py :

SYNTAX :

python apriori_test.py [data_set_file_location] [minimum_support_count]

Note : 0 <= minimum_support_count <= 1

EXAMPLE :

python apriori_test.py data_set.txt 0.6