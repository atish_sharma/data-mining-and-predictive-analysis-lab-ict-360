from apriori import apriori
import sys

data_source=open(sys.argv[1])
data=data_source.read().strip().split('\n')
data_source.close()
transactions=[]
for i in data:
	t=i.split(',')
	for j in range(len(t)):
		t[j]=t[j].strip() 
	transactions.append(t)
apr=apriori(transactions,float(sys.argv[2]))
frequent_itemsets=apr.generate_frequent_itemsets()
frequent_itemsets.sort(key=lambda a:len(a))
for i in frequent_itemsets:
	print(i)

