import sys,ast,k_means

data=ast.literal_eval(open(sys.argv[1]).read().strip())
cluster=k_means.k_means(data['objects'],data['centers'],data['k'],data['random'])
cluster.cluster()