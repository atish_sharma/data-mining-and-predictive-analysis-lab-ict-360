k_means.py : contains k_means class.
k_means_test.py : instantiates an object of k_means class and feeds it input from the command line.
data_set.json : example data set with a list of objects and clustering configuration.

How to run k_means_test.py :

SYNTAX :

python k_means_test.py [data_set_file_location]

EXAMPLE :

python k_means_test.py data_set.json