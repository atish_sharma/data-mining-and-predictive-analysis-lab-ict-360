import copy,math,pprint,random

class k_means:
	
	def __init__(self,objects,centers,k=0,randomized=False):
		self.obj=objects
		self.center=[]
		if randomized:
			l=range(len(objects))
			for i in range(k):
				n=random.randint(0,len(l)-1)
				self.center.append(copy.deepcopy(objects[l[n]]))
				del l[n]
		else:
			for c in centers:
				for o in objects:
					if o['id']==c:
						self.center.append(copy.deepcopy(o))
						break

	def cluster(self):
		count=1
		while True:
			self.write('CENTER {0}'.format(count),self.center)
			dist=self.distance()
			self.write('DISTANCE {0}'.format(count),dist)
			least_dist=self.least_distance(dist)
			self.write('LEAST DISTANCE {0}'.format(count),least_dist)
			centroid=self.centroids(least_dist)
			if self.equal_centroids(centroid):
				self.center={}
				count=1
				for i in least_dist.keys():
					key='Cluster #{0}'.format(count)
					self.center[key]=[]
					for j in least_dist[i].keys():
						if least_dist[i][j]:
							self.center[key].append(j)
					self.center[key].sort()
					count+=1
				break
			else:
				self.center=centroid
			count+=1
		self.write('FINAL CLUSTERS',self.center)

	def write(self,heading,data):
		div='-'*len(heading)
		print(div+'\n'+heading+'\n'+div)
		pprint.pprint(data)

	def distance(self):
		dist={}
		for c in self.center:
			dist[c['id']]={}
			for o in self.obj:
				dist[c['id']][o['id']]=self.eucledian_distance(o['attributes'],c['attributes'])
		return dist

	def centroids(self,least_dist):
		centroid=copy.deepcopy(self.center)
		for c in centroid:
			for k in c['attributes'].keys():
				c['attributes'][k]=0.0
			c['points']=0
			for l in least_dist[c['id']].keys():
				if least_dist[c['id']][l]:
					for k in self.obj:
						if k['id']==l:
							for attr in c['attributes'].keys():
								c['attributes'][attr]+=k['attributes'][attr]
							c['points']+=1
							break
			for k in c['attributes'].keys():
				if c['points']!=0:
					c['attributes'][k]/=c['points']
		return centroid

	def equal_centroids(self,centroid):
		for c in centroid:
			for center in self.center:
				if center['id']==c['id']:
					for k in c['attributes'].keys():
						if '{:.2f}'.format(c['attributes'][k])!='{:.2f}'.format(center['attributes'][k]):
							return False
					break
		return True

	def least_distance(self,dist):
		least_dist={}
		center_keys=dist.keys()
		point_keys=dist[center_keys[0]].keys()
		for i in center_keys:
			least_dist[i]={}
			for j in point_keys:
				least_dist[i][j]=False
		for i in point_keys:
			min=center_keys[0]
			for j in center_keys:
				if dist[j][i]<dist[min][i]:
					min=j
			least_dist[min][i]=True
		return least_dist
			
	def eucledian_distance(self,point,center):
		dist=0.0
		for key,val in center.items():
			dist+=(val-point[key])*(val-point[key])
		dist=math.sqrt(dist)
		return dist