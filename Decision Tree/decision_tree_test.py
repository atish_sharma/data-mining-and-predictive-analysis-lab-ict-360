from decision_tree import decision_tree
import sys

data_source=open(sys.argv[1])
label_pos=int(sys.argv[2])
data=data_source.read().strip().split('\n')
data_source.close()
head=[]
for i in data[0].split(','):
	head.append(i.strip())
label=head[label_pos]
del data[0]
table=[]
for i in data:
	k=i.split(',')
	a=[]
	for j in k:
		a.append(j.strip())
	table.append(a)
id3=decision_tree(head,label,table)
id3.build_decision_tree()
id3.show_tree()
while True:
	r=raw_input('Predict? [y/n] ')
	if r=='n':
		break
	id3.predict()
