import copy,math,pprint

class decision_tree:

	def __init__ (self,head,label,table):
		self.head=head
		self.label=label
		self.table=table
		self.domain={}
		for i in range(len(self.head)):
			s=set()
			for m in range(len(self.table)):
				for n in range(len(self.table[m])):
					if n==i:
						s.add(self.table[m][n])
			self.domain[self.head[i]]=s
		self.tree=None

	def build_decision_tree(self):
		self.tree=self.build_decision_sub_tree(self.head,self.table)

	def show_tree(self):
		d=self.tree_to_dict(self.tree)
		r=100
		print('-'*r)
		print('DECISION TREE')
		print('-'*r)
		pprint.pprint(d)
		print('-'*r)

	def tree_to_dict(self,n):
		if n==None:
			return None
		d={}
		l=n.get_label()
		d[l]={}
		for i in n.get_edges():
			d[l][i.get_value()]=self.tree_to_dict(i.get_next())
		if not d[l]:
			return l
		return d

	def predict(self):
		a={}
		for i in self.head:
			if i==self.label:
				continue
			print 'Enter "'+i+'"'
			print 'Domain : ',list(self.domain[i])
			a[i]=raw_input()
		h=self.tree
		while True:
			e=h.get_edges()
			if len(e)==0:
				print(h.get_label())
				break
			else:
				for i in e:
					if a[h.get_label()]==i.get_value():
						h=i.get_next()
						break			
		r=100
		print('-'*r)

	def build_decision_sub_tree(self,head,table):
		a=self.get_max_gain(head,table)
		n=self.split(head,table,a)
		e=n.get_edges()
		for i in range(len(e)):
			nxt=e[i].get_next()
			e[i].set_next(self.build_decision_sub_tree(nxt['head'],nxt['table']))
		return n

	def get_max_gain(self,head,table):
		gain=0.0
		a=None
		for i in head:
			if i!=self.label:
				a=i
				break
		if a==None:
			return self.label
		info=self.info_d(head,table)
		for i in head:
			if i==self.label:
				continue
			g=info-self.info_a(head,table,i)
			if g>gain:
				gain=g
				a=i
		return a

	def split(self,head,table,attr):
		head=self.deep_copy(head)
		table=self.deep_copy(table)
		n=self.is_leaf_node(head,table)
		if n==None:
			n=node(attr)
		else:
			return n
		splits={}
		for i in self.domain[attr]:
			splits[i]={'head':[],'table':[]}
		index=self.get_attr_index(head,attr)
		for i in range(len(table)):
			a=[]
			for j in range(len(table[i])):
				if j!=index:
					a.append(table[i][j])
			splits[table[i][index]]['table'].append(a)
		del head[index]
		for i in self.domain[attr]:
			e=edge(attr,i)
			n.add_edge(e)
			splits[i]['head']=head
			e.set_next(splits[i])
		return n

	def is_leaf_node(self,head,table):
		index=self.get_attr_index(head,self.label)
		prev=table[0][index]
		for i in range(len(table)):
			for j in range(len(table[i])):
				if j==index:
					if prev!=table[i][j]:
						return None
					else:
						prev=table[i][j]
		return node(prev)

	def get_attr_index(self,head,attr):
		for i in range(len(head)):
			if attr==head[i]:
				return i
		return -1

	def deep_copy(self,obj):
		return copy.deepcopy(obj)

	def info_a(self,head,table,attr):
		v=0.0
		ac=self.attr_count(table)
		for i in self.domain[attr]:
			value=i
			avc=self.attr_val_count(head,table,attr,value)
			val=0.0
			for j in self.domain[self.label]:
				label=j
				d=0.0
				try:
					d=float(avlc)/float(avc)
				except:
					d=0.0
				avlc=self.attr_val_label_count(head,table,attr,value,label)
				val+=(-1.0)*d*self.log(d)
			d=0.0
			try:
				d=float(avc)/float(ac)
			except:
				d=0.0
			val*=d
			v+=val
		return v

	def info_d(self,head,table):
		v=0.0
		ac=self.attr_count(table)
		for i in self.domain[self.label]:
			avc=self.attr_val_count(head,table,self.label,i)
			d=0.0
			try:
				d=float(avc)/float(ac)
			except:
				d=0.0
			nv=(-1.0)*d*self.log(d)
			v+=nv
		return v

	def attr_count(self,table):
		return len(table)

	def attr_val_count(self,head,table,attr,val):
		c=0
		index=self.get_attr_index(head,attr)
		for i in range(len(table)):
			if table[i][index]==val:
				c+=1
		return c

	def attr_val_label_count(self,head,table,attr,val,label_val):
		c=0
		index=self.get_attr_index(head,attr)
		index_l=self.get_attr_index(head,self.label)
		for i in range(len(table)):
			if table[i][index_l]==label_val and table[i][index]==val:
				c+=1
		return c

	def log(self,v):
		if v<=0.0:
			return 0.0
		return math.log(v,2)

class node:

	def __init__(self,label):
		self.label=label
		self.edge_list=[]

	def get_edges(self):
		return self.edge_list

	def set_edges(self,e):
		self.edge_list=e

	def get_label(self):
		return self.label

	def add_edge(self,e):
		self.edge_list.append(e)

class edge:

	def __init__(self,label,value):
		self.label=label
		self.value=value
		self.next=None

	def set_next(self,n):
		self.next=n

	def get_next(self):
		return self.next

	def get_label(self):
		return self.label

	def get_value(self):
		return self.value

