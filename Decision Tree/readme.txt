decision_tree.py : contains decision_tree, node and edge class.
decision_tree_test.py : instantiates an object of decision_tree class and feeds it input from the command line.
data.txt : example data set, a list of tuples in tabular format.

How to run decision_tree_test.py :

SYNTAX :

python decision_tree_test.py [location_of_data_set] [index_of_label_column]

EXAMPLE :

python decision_tree_test.py data.txt 2
